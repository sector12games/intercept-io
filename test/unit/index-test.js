/* global it, describe */
/* eslint prefer-arrow-callback: "off" */

const chai = require('chai');
const interceptIO = require('../../src/index');

const promiseResolveDuration = 1000;

describe('index-test.js', function () {
    describe('interceptStdoutAndStderr', function () {
        it('should intercept stdio and stderr', function () {
            const result = interceptIO.interceptStdoutAndStderr(function () {
                console.log('out1');
                console.log('out2');
                console.error('err1');
                console.error('err2');
            });

            chai.assert.includeMembers(result.stdoutResult, ['out1\n', 'out2\n']);
            chai.assert.includeMembers(result.stderrResult, ['err1\n', 'err2\n']);
            chai.assert.include(result.all, 'out1');
            chai.assert.include(result.all, 'out2');
            chai.assert.include(result.all, 'err1');
            chai.assert.include(result.all, 'err2');
        });
    });

    describe('interceptStdoutAndStderrAsync', function () {
        this.slow(50000);
        this.timeout(10000);

        it('should immediately reject when function provided is undefined', function (done) {
            interceptIO.interceptStdoutAndStderrAsync(undefined)
            .then(function (result) {
                done(new Error('Exception should have been thrown, but was not.'));
            }).catch(function (error) {
                chai.assert.equal(error.message, 'promiseFunctionToExecute is not a function');
                done();
            });
        });

        it('should immediately reject when function provided does not return a promise', function (done) {
            interceptIO.interceptStdoutAndStderrAsync(() => {
                console.log('test');
                console.error('test');
            }).then(function (result) {
                done(new Error('Exception should have been thrown, but was not.'));
            }).catch(function (error) {
                chai.assert.equal(error.message, 'Cannot read property \'then\' of undefined');
                done();
            });
        });

        it('should intercept stdio and stderr across all async calls', function (done) {
            interceptIO.interceptStdoutAndStderrAsync(function () {
                const promise = new Promise((resolve) => {
                    // Resolve the promise after a few seconds
                    setTimeout(function () {
                        console.log('out1');
                        console.log('out2');
                        console.error('err1');
                        console.error('err2');

                        resolve('Success.');
                    }, promiseResolveDuration);
                });
                return promise;
            }).then(function (result) {
                chai.assert.includeMembers(result.stdoutResult, ['out1\n', 'out2\n']);
                chai.assert.includeMembers(result.stderrResult, ['err1\n', 'err2\n']);
                chai.assert.include(result.all, 'out1');
                chai.assert.include(result.all, 'out2');
                chai.assert.include(result.all, 'err1');
                chai.assert.include(result.all, 'err2');

                done();
            }).catch(function (error) {
                done(error);
            });
        });

        it('should restore original stdio when promise resolves', function (done) {
            interceptIO.interceptStdoutAndStderrAsync(function () {
                const promise = new Promise((resolve) => {
                    // Resolve the promise after a few seconds
                    setTimeout(function () {
                        console.log('out1');
                        console.error('err1');

                        resolve('Success.');
                    }, promiseResolveDuration);
                });
                return promise;
            }).then(function (result) {
                console.log('\ttest stdout should not be intercepted (ignore)');
                console.error('\ttest stderr should not be intercepted (ignore)');

                chai.assert.includeMembers(result.stdoutResult, ['out1\n']);
                chai.assert.includeMembers(result.stderrResult, ['err1\n']);
                chai.assert.include(result.all, 'out1');
                chai.assert.include(result.all, 'err1');

                chai.assert.notInclude(result.all, 'test stdout should not be intercepted (ignore)');
                chai.assert.notInclude(result.all, 'test stderr should not be intercepted (ignore)');

                done();
            }).catch(function (error) {
                done(error);
            });
        });

        it('should restore original stdio when promise rejects', function (done) {
            interceptIO.interceptStdoutAndStderrAsync(function () {
                const promise = new Promise((resolve, reject) => {
                    // Resolve the promise after a few seconds
                    setTimeout(function () {
                        console.log('out1');
                        console.error('err1');

                        reject('Failure.');
                    }, promiseResolveDuration);
                });
                return promise;
            }).then(function (result) {
                console.log('\ttest stdout should not be intercepted (ignore)');
                console.error('\ttest stderr should not be intercepted (ignore)');

                chai.assert.includeMembers(result.stdoutResult, ['out1\n']);
                chai.assert.includeMembers(result.stderrResult, ['err1\n']);
                chai.assert.include(result.all, 'out1');
                chai.assert.include(result.all, 'err1');

                chai.assert.notInclude(result.all, 'test stdout should not be intercepted (ignore)');
                chai.assert.notInclude(result.all, 'test stderr should not be intercepted (ignore)');

                done();
            }).catch(function (error) {
                done(error);
            });
        });
    });

    describe('interceptStdout', function () {
        it('should intercept stdio and not stderr', function () {
            const result = interceptIO.interceptStdout(function () {
                console.log('out1');
                console.log('out2');
                console.error('\terr1 (ignore)');
            });

            chai.assert.includeMembers(result.stdoutResult, ['out1\n', 'out2\n']);
            chai.assert.isNull(result.stderrResult);
            chai.assert.include(result.all, 'out1');
            chai.assert.include(result.all, 'out2');
            chai.assert.notInclude(result.all, 'err1');
        });
    });

    describe('interceptStdoutAsync', function () {
        this.slow(50000);
        this.timeout(10000);

        it('should immediately reject when function provided is undefined', function (done) {
            interceptIO.interceptStdoutAsync(undefined)
            .then(function (result) {
                done(new Error('Exception should have been thrown, but was not.'));
            }).catch(function (error) {
                chai.assert.equal(error.message, 'promiseFunctionToExecute is not a function');
                done();
            });
        });

        it('should immediately reject when function provided does not return a promise', function (done) {
            interceptIO.interceptStdoutAsync(() => {console.log('test');})
            .then(function (result) {
                done(new Error('Exception should have been thrown, but was not.'));
            }).catch(function (error) {
                chai.assert.equal(error.message, 'Cannot read property \'then\' of undefined');
                done();
            });
        });

        it('should intercept stdio across all async calls', function (done) {
            interceptIO.interceptStdoutAsync(function () {
                const promise = new Promise((resolve) => {
                    // Resolve the promise after a few seconds
                    setTimeout(function () {
                        console.log('out1');
                        console.log('out2');

                        resolve('Success.');
                    }, promiseResolveDuration);
                });
                return promise;
            }).then(function (result) {
                chai.assert.includeMembers(result.stdoutResult, ['out1\n', 'out2\n']);
                chai.assert.include(result.all, 'out1');
                chai.assert.include(result.all, 'out2');

                done();
            }).catch(function (error) {
                done(error);
            });
        });

        it('should restore original stdio when promise resolves', function (done) {
            interceptIO.interceptStdoutAsync(function () {
                const promise = new Promise((resolve) => {
                    // Resolve the promise after a few seconds
                    setTimeout(function () {
                        console.log('out1');

                        resolve('Success.');
                    }, promiseResolveDuration);
                });
                return promise;
            }).then(function (result) {
                console.log('\ttest stdout should not be intercepted (ignore)');

                chai.assert.includeMembers(result.stdoutResult, ['out1\n']);
                chai.assert.include(result.all, 'out1');
                chai.assert.notInclude(result.all, 'test stdout should not be intercepted (ignore)');

                done();
            }).catch(function (error) {
                done(error);
            });
        });

        it('should restore original stdio when promise rejects', function (done) {
            interceptIO.interceptStdoutAsync(function () {
                const promise = new Promise((resolve, reject) => {
                    // Resolve the promise after a few seconds
                    setTimeout(function () {
                        console.log('out1');

                        reject('Failure.');
                    }, promiseResolveDuration);
                });
                return promise;
            }).then(function (result) {
                console.log('\ttest stdout should not be intercepted (ignore)');

                chai.assert.includeMembers(result.stdoutResult, ['out1\n']);
                chai.assert.include(result.all, 'out1');
                chai.assert.notInclude(result.all, 'test stdout should not be intercepted (ignore)');

                done();
            }).catch(function (error) {
                done(error);
            });
        });
    });

    describe('interceptStderr', function () {
        it('should intercept stderr and not stdio', function () {
            const result = interceptIO.interceptStderr(function () {
                console.log('\tout1 (ignore)');
                console.error('err1');
                console.error('err2');
            });

            chai.assert.isNull(result.stdoutResult);
            chai.assert.includeMembers(result.stderrResult, ['err1\n', 'err2\n']);
            chai.assert.notInclude(result.all, 'out1');
            chai.assert.include(result.all, 'err1');
            chai.assert.include(result.all, 'err2');
        });
    });

    describe('interceptStderrAsync', function () {
        this.slow(50000);
        this.timeout(10000);

        it('should immediately reject when function provided is undefined', function (done) {
            interceptIO.interceptStderrAsync(undefined)
            .then(function (result) {
                done(new Error('Exception should have been thrown, but was not.'));
            }).catch(function (error) {
                chai.assert.equal(error.message, 'promiseFunctionToExecute is not a function');
                done();
            });
        });

        it('should immediately reject when function provided does not return a promise', function (done) {
            interceptIO.interceptStderrAsync(() => {console.error('test');})
            .then(function (result) {
                done(new Error('Exception should have been thrown, but was not.'));
            }).catch(function (error) {
                chai.assert.equal(error.message, 'Cannot read property \'then\' of undefined');
                done();
            });
        });

        it('should intercept stderr across all async calls', function (done) {
            interceptIO.interceptStderrAsync(function () {
                const promise = new Promise((resolve) => {
                    // Resolve the promise after a few seconds
                    setTimeout(function () {
                        console.error('err1');
                        console.error('err2');

                        resolve('Success.');
                    }, promiseResolveDuration);
                });
                return promise;
            }).then(function (result) {
                chai.assert.includeMembers(result.stderrResult, ['err1\n', 'err2\n']);
                chai.assert.include(result.all, 'err1');
                chai.assert.include(result.all, 'err2');

                done();
            }).catch(function (error) {
                done(error);
            });
        });

        it('should restore original stdio when promise resolves', function (done) {
            interceptIO.interceptStderrAsync(function () {
                const promise = new Promise((resolve) => {
                    // Resolve the promise after a few seconds
                    setTimeout(function () {
                        console.error('err1');

                        resolve('Success.');
                    }, promiseResolveDuration);
                });
                return promise;
            }).then(function (result) {
                console.error('\ttest stderr should not be intercepted (ignore)');

                chai.assert.includeMembers(result.stderrResult, ['err1\n']);
                chai.assert.include(result.all, 'err1');
                chai.assert.notInclude(result.all, 'test stderr should not be intercepted (ignore)');

                done();
            }).catch(function (error) {
                done(error);
            });
        });

        it('should restore original stdio when promise rejects', function (done) {
            interceptIO.interceptStderrAsync(function () {
                const promise = new Promise((resolve, reject) => {
                    // Resolve the promise after a few seconds
                    setTimeout(function () {
                        console.error('err1');

                        reject('Failure.');
                    }, promiseResolveDuration);
                });
                return promise;
            }).then(function (result) {
                console.error('\ttest stderr should not be intercepted (ignore)');

                chai.assert.includeMembers(result.stderrResult, ['err1\n']);
                chai.assert.include(result.all, 'err1');
                chai.assert.notInclude(result.all, 'test stderr should not be intercepted (ignore)');

                done();
            }).catch(function (error) {
                done(error);
            });
        });
    });
});
