const stdout = require('test-console').stdout;
const stderr = require('test-console').stderr;

/**
 * A helper function that allows us to intercept stdout (console.log) statements,
 * and return the result as an array of strings rather than printing the string
 * to the console. This allows us to verify in our unit/integration tests that
 * the console output is correct, but does not clutter up the mocha reporter by
 * having console output mixed in.
 * I originally attempted this same logic using beforeEach() and afterEach() mocha
 * hooks, but this caused the reporter to break (the reporter for each test writes
 * to the console before the afterEach() method is actually called, so the reporter
 * text was intercepted as well).
 * More info:
 * http://stackoverflow.com/questions/18543047/mocha-monitor-application-output
 *
 * @param {function} functionToExecute - The function to call while intercepting
 *      stdout and stderr.
 * @returns {Object} All intercepted stdout and stderr messages in array form,
 *      and all messages joined together as a single string.
 */
function interceptStdoutAndStderr(functionToExecute) {
    // Run the function and capture both stdout and stderr input. Make sure to
    // restore standard stdout/stderr functionality regardless of whether or not
    // an error occurred during the function call.
    const stdoutResult = stdout.inspect();
    const stderrResult = stderr.inspect();
    try {
        functionToExecute();
    } finally {
        stdoutResult.restore();
        stderrResult.restore();
    }

    // Each result is an array of strings, one for each console.log/info/error/etc
    // statement that occurred within functionToExecute. Line break characters are
    // included.
    return {
        stdoutResult: stdoutResult.output,
        stderrResult: stderrResult.output,
        all: stdoutResult.output.join() + stderrResult.output.join(),
    };
}

/**
 * Intercepts stdout and stderr messages until the async method passed in has completed.
 * Be careful when using this in code that executes multiple functions in parallel -
 * stderr and stdout will be intercepted for all functions until the function passed
 * in has completed.
 *
 * @param {function} promiseFunctionToExecute - The function to call while intercepting
 *      stdout and stderr. The function provided must return a promise.
 * @returns {Promise<Object>} A promise that eventually resolves to all intercepted
 *      stdout and stderr messages in array form, and all messages joined together as
 *      a single string.
 */
function interceptStdoutAndStderrAsync(promiseFunctionToExecute) {
    // Run the function and capture both stdout and stderr input. Make sure to
    // restore standard stdout/stderr functionality regardless of whether or not
    // an error occurred during the function call.
    const stdoutResult = stdout.inspect();
    const stderrResult = stderr.inspect();
    try {
        const promise = promiseFunctionToExecute().then(() => {
            stdoutResult.restore();
            stderrResult.restore();

            // Each result is an array of strings, one for each console.log/info/error/etc
            // statement that occurred within functionToExecute. Line break characters are
            // included.
            return {
                stdoutResult: stdoutResult.output,
                stderrResult: stderrResult.output,
                all: stdoutResult.output.join() + stderrResult.output.join(),
            };
        }).catch((error) => {
            stdoutResult.restore();
            stderrResult.restore();

            // Each result is an array of strings, one for each console.log/info/error/etc
            // statement that occurred within functionToExecute. Line break characters are
            // included.
            return {
                stdoutResult: stdoutResult.output,
                stderrResult: stderrResult.output,
                all: stdoutResult.output.join() + stderrResult.output.join(),
            };
        });

        return promise;
    } catch(e) {
        // If there's a problem simply calling the function (function doesn't exist doesn't
        // return a promise, etc) then we just want to reject with the error. Unlike the
        // sync version of this method, the function provided MUST be valid and return a
        // promise.
        stdoutResult.restore();
        stderrResult.restore();
        return Promise.reject(e);
    }
}

/**
 * Intercept stdout only
 *
 * @param {function} functionToExecute - The function to call while intercepting
 *      stdout and stderr.
 * @returns {Object} All intercepted stdout messages in array form,
 *     and all messages joined together as a single string.
 */
function interceptStdout(functionToExecute) {
    const stdoutResult = stdout.inspect();
    try {
        functionToExecute();
    } finally {
        stdoutResult.restore();
    }

    return {
        stdoutResult: stdoutResult.output,
        stderrResult: null,
        all: stdoutResult.output.join(),
    };
}

/**
 * Intercepts stdout messages until the async method passed in has completed. Be careful
 * when using this in code that executes multiple functions in parallel - stdout will
 * be intercepted for all functions until the function passed in has completed.
 *
 * @param {function} promiseFunctionToExecute - The function to call while intercepting
 *      stdout. The function provided must return a promise.
 * @returns {Promise<Object>} A promise that eventually resolves to all intercepted
 *      stdout messages in array form, and all messages joined together as a single string.
 */
function interceptStdoutAsync(promiseFunctionToExecute) {
    const stdoutResult = stdout.inspect();
    try {
        const promise = promiseFunctionToExecute().then(() => {
            stdoutResult.restore();

            return {
                stdoutResult: stdoutResult.output,
                stderrResult: null,
                all: stdoutResult.output.join(),
            };
        }).catch((error) => {
            stdoutResult.restore();
            return {
                stdoutResult: stdoutResult.output,
                stderrResult: null,
                all: stdoutResult.output.join(),
            };
        });

        return promise;
    } catch(e) {
        // If there's a problem simply calling the function (function doesn't exist doesn't
        // return a promise, etc) then we just want to reject with the error. Unlike the
        // sync version of this method, the function provided MUST be valid and return a
        // promise.
        stdoutResult.restore();
        return Promise.reject(e);
    }
}

/**
 * Intercept stderr only
 *
 * @param {function} functionToExecute - The function to call while intercepting
 *      stdout and stderr.
 * @returns {Object} All intercepted stderr messages in array form,
 *     and all messages joined together as a single string.
 */
function interceptStderr(functionToExecute) {
    const stderrResult = stderr.inspect();
    try {
        functionToExecute();
    } finally {
        stderrResult.restore();
    }

    return {
        stdoutResult: null,
        stderrResult: stderrResult.output,
        all: stderrResult.output.join(),
    };
}

/**
 * Intercepts stderr messages until the async method passed in has completed. Be careful
 * when using this in code that executes multiple functions in parallel - stderr will
 * be intercepted for all functions until the function passed in has completed.
 *
 * @param {function} promiseFunctionToExecute - The function to call while intercepting
 *      stderr. The function provided must return a promise.
 * @returns {Promise<Object>} A promise that eventually resolves to all intercepted
 *      stderr messages in array form, and all messages joined together as a single string.
 */
function interceptStderrAsync(promiseFunctionToExecute) {
    const stderrResult = stderr.inspect();
    try {
        const promise = promiseFunctionToExecute().then(() => {
            stderrResult.restore();

            return {
                stdoutResult: null,
                stderrResult: stderrResult.output,
                all: stderrResult.output.join(),
            };
        }).catch((error) => {
            stderrResult.restore();
            return {
                stdoutResult: null,
                stderrResult: stderrResult.output,
                all: stderrResult.output.join(),
            };
        });

        return promise;
    } catch(e) {
        // If there's a problem simply calling the function (function doesn't exist doesn't
        // return a promise, etc) then we just want to reject with the error. Unlike the
        // sync version of this method, the function provided MUST be valid and return a
        // promise.
        stderrResult.restore();
        return Promise.reject(e);
    }
}

module.exports = {
    interceptStdoutAndStderrAsync,
    interceptStdoutAsync,
    interceptStderrAsync,
    interceptStdoutAndStderr,
    interceptStdout,
    interceptStderr,
};
