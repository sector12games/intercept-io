## intercept-io

A set of helper functions that allow you to intercept stdout (console.log)
and stderr (console.error) statements, and return them to the caller rather
than printing to the console.

Useful for unit testing.
